package com.projects.deadlock;

public class Main {

    /**
     * lock object
     */
    public static final String LOCK_1 = "Lock 1";

    /**
     * lock object
     */
    public static final String LOCK_2 = "Lock 2";

    public static void main(String[] args) {
        boolean lockFlag = true;

        if(args.length > 0)
            lockFlag = Boolean.parseBoolean(args[0]);


        TestThread firstThread = new TestThread()
                .setFirstLock(LOCK_1)
                .setSecondLock(LOCK_2)
                .setLockFlag(lockFlag);

        TestThread secondThread = new TestThread()
                .setFirstLock(LOCK_2)
                .setSecondLock(LOCK_1)
                .setLockFlag(lockFlag);

        firstThread.start();
        secondThread.start();
    }

    /**
     * class for experimental thread
     */
    private static class TestThread extends Thread {

        /**
         * flag for deadlock
         */
        private boolean lockFlag;

        /**
         * lock object
         */
        private Object firstLock;

        /**
         * lock object
         */
        private Object secondLock;

        /**
         * lockFlag setter
         * @param lookFlag - new flag value
         * @return this thread
         */
        public TestThread setLockFlag(boolean lookFlag){
            this.lockFlag = lookFlag;

            return this;
        }

        /**
         * first lock object setter
         * @param lock - lock object
         * @return this thread
         */
        public TestThread setFirstLock(Object lock){
            this.firstLock = lock;

            return this;
        }

        /**
         * second lock object setter
         * @param lock - lock object
         * @return this thread
         */
        public TestThread setSecondLock(Object lock){
            this.secondLock = lock;

            return this;
        }


        /**
         * Main thread function
         */
        public void run() {
            synchronized (firstLock) {
                log("Holding '%s'".formatted(firstLock));
                sleep(10);

                if(!lockFlag){
                    log("Waiting 60 sec");
                    sleep(1000 * 60);
                    return;
                }

                log("Waiting for '%s'".formatted(secondLock));

                synchronized (secondLock) {
                    log("Holding lock 1 & 2...");
                }
            }
        }

        /**
         * Puts stream to sleep
         * @param millis - time in millis
         */
        private void sleep(int millis){
            try {
                Thread.sleep(millis);
            }
            catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        /**
         * Log information
         * @param message - text for logging
         */
        private void log(String message){
            System.out.println(Thread.currentThread().getName() + ": " + message);
        }
    }
}
